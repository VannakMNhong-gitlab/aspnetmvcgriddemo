CREATE TABLE [dbo].Employees
(
	EmployeeID INT NOT NULL PRIMARY KEY Identity,
	BadgeNo varchar(50) not null,
	FirstName varchar(50) not null,
	LastName varchar(50),
	Email varchar(250),
	City varchar(50),
	State varchar(50),
	Status varchar(50),
)
--EmployeeBadgeID varchar(255),
INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State, 
	Status
)
VALUES
(   N'i111', -- BadgeNo - varchar(50)
    N'Michael', -- FirstName - varchar(50)
    N'Douglas', -- LastName - varchar(50)
    N'Michael.Douglas@armc.sbcounty.gov', -- Email - varchar(250)
    N'San Bernardino', -- City - varchar(50)
    N'CA',  -- Country - varchar(50)
	N''
)

INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State,
	Status
)
VALUES
(   N'i112', -- BadgeNo - varchar(50)
    N'Michael', -- FirstName - varchar(50)
    N'James', -- LastName - varchar(50)
    N'Michael.James@armc.sbcounty.gov', -- Email - varchar(250)
    N'San Bernardino', -- City - varchar(50)
    N'CA',  -- Country - varchar(50)
	N''
)
INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State,
	Status
)
VALUES
(   N'i113', -- BadgeNo - varchar(50)
    N'Suzan', -- FirstName - varchar(50)
    N'Thomson', -- LastName - varchar(50)
    N'Suzan.Tomson@armc.sbcounty.gov', -- Email - varchar(250)
    N'San Bernardino', -- City - varchar(50)
    N'CA',  -- Country - varchar(50)
	N''
)

SELECT * FROM Employees
--DROP TABLE Employees