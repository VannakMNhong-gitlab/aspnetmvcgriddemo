CREATE TABLE [dbo].Employees
(
	EmployeeID INT NOT NULL PRIMARY KEY Identity,
	BadgeNo nvarchar(50) not null,
	FirstName nvarchar(50) not null,
	LastName nvarchar(50),
	Email nvarchar(250),
	City nvarchar(50),
	State nvarchar(50)
)
INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State
)
VALUES
(   N'i111', -- BadgeNo - nvarchar(50)
    N'Michael', -- FirstName - nvarchar(50)
    N'Douglas', -- LastName - nvarchar(50)
    N'Michael.Douglas@armc.sbcounty.gov', -- Email - nvarchar(250)
    N'San Bernardino', -- City - nvarchar(50)
    N'CA'  -- Country - nvarchar(50)
)

INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State
)
VALUES
(   N'i112', -- BadgeNo - nvarchar(50)
    N'Michael', -- FirstName - nvarchar(50)
    N'James', -- LastName - nvarchar(50)
    N'Michael.James@armc.sbcounty.gov', -- Email - nvarchar(250)
    N'San Bernardino', -- City - nvarchar(50)
    N'CA'  -- Country - nvarchar(50)
)
INSERT INTO dbo.Employees
(
    BadgeNo,
    FirstName,
    LastName,
    Email,
    City,
    State
)
VALUES
(   N'i111', -- BadgeNo - nvarchar(50)
    N'Suzan', -- FirstName - nvarchar(50)
    N'Thomson', -- LastName - nvarchar(50)
    N'Suzan.Tomson@armc.sbcounty.gov', -- Email - nvarchar(250)
    N'San Bernardino', -- City - nvarchar(50)
    N'CA'  -- Country - nvarchar(50)
)
UPDATE Employees SET BadgeNo='i113' where EmployeeID=3

SELECT * FROM Employees
--DROP TABLE Employees