﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ASPNETMvcGridDemo.Models
{
    /// <summary>
    /// NOTE:  This is my Partial View class, where I will apply the data
    /// validation on the server side.  This partial class is an extention model of the Employee class, 
    /// where it drives from the database - data generate model
    /// 
    /// </summary>
    [MetadataType(typeof(EmployeeMetadata))]
    public partial class Employee
    {
    }
    /// <summary>
    /// Applying the server validation with, data attribution 
    /// </summary>
    public class EmployeeMetadata
    {
        [Required (AllowEmptyStrings = false, ErrorMessage = "Please provide Employee Badge number")]
        public string BadgeNo { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide Employee first name")]
        public string FirstName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Please provide Employee last name")]
        public string LastName { get; set; }
    }
}