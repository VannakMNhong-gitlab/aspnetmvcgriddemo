﻿using ASPNETMvcGridDemo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASPNETMvcGridDemo.Controllers
{
    public class HomeController : Controller
    {
        //TODO:  Add menu - future expand....or create another controller...
        public ActionResult Action1(int id)
        {
            return View();
        }
        public ActionResult Action2(int id, string role)
        {
            return View();
        }
        /* ---------------------------------------------------------------------------------------------- */
        //This index show data table in the index view
        public ActionResult Index()
        {
            return View();  //Return to the UI view =>/home/index 
        }
        public ActionResult ErrorHandling()
        {
            return View();
        }
        
        /// <summary>
        /// NOTE:  I am display the data contents from the database table calls Employees
        /// I am using jQuery with ajax call, return this back to the UI with json, 
        /// when the "url": '/home/GetEmployees' is call.  Using LINQ orderby first name
        /// </summary>
        /// <returns></returns>
        public ActionResult GetEmployees()
        {
            //NOTE: this try catch handling basic to prevent the application crash, when there database table is found
            // return error message will handling at the client level during the ajax call to the controller
            //my validations are being handling at the client and at the server in the EmployeeMetadata
            try
            {
                using (MVNDatabaseEntities dc = new MVNDatabaseEntities())
                {               
                    var employees = dc.Employees.OrderBy(a => a.FirstName).ToList();       
                    return Json(new { data = employees }, JsonRequestBehavior.AllowGet);    //Return data contents in gridView 
                }
            }
            catch (Exception e)
            {
                return View("ErrorHandling", new HandleErrorInfo(e, "GetEmployees", "ErrorHandling")); //return view NotFound/Controller/Action
            }
        }
        /// <summary>
        /// NOTE:  using the MVC action result, when UI getting the employee information, with specific ID,
        /// To do the CRUD process..>Create Read Update and Delete 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]  //getting action (Read) 
        public ActionResult AddOrEdit(int id)
        {
            //NOTE: this try catch handling basic to prevent the application crash, when there database table is found
            // return error message will handling at the client level during the ajax call to the controller 
            try
            {
                //if(id == 0)
                //{
                //    return View("NotFound");  //Return to View name NotFound
                //}
                using (MVNDatabaseEntities dc = new MVNDatabaseEntities())
                {
                    //Returns the first element of the sequence that satisfies a condition or a default value if no such element is found.
                    var e = dc.Employees.Where(a => a.EmployeeID == id).FirstOrDefault(); //first item or null
                    return View(e);  //NOTE:  Returning employee detail 
                }
            }
            catch(Exception e)
            {
                return View("ErrorHandling", new HandleErrorInfo(e, "GetEmployees", "ErrorHandling")); //return view NotFound/Controller/Action
            }
            
        }
        //POST data back to the server ->database
        [HttpPost]
        public ActionResult AddOrEdit(Employee emp)
        {
            bool status = false;
            //ModelState.IsValid indicates if it was possible to bind the incoming values from the request 
            //to the model correctly and whether any explicitly specified validation rules were broken during the model binding process.
            if (ModelState.IsValid)
            {
                using(MVNDatabaseEntities dc = new MVNDatabaseEntities())
                {
                    if (dc == null)
                    {
                        return View("NotFound");  //Return the Not found view
                    }
                    else
                    {
                        if (emp.EmployeeID > 0)
                        {
                            //Edit - Returns the first element of the sequence that satisfies a condition or a default value if no such element is found.
                            var e = dc.Employees.Where(a => a.EmployeeID == emp.EmployeeID).FirstOrDefault(); //first item or null
                                                                                                              //Add new
                            if (e != null)
                            {
                                e.BadgeNo = emp.BadgeNo;
                                e.FirstName = emp.FirstName;
                                e.LastName = emp.LastName;
                                e.Email = emp.Email;
                                e.City = emp.City;
                                e.State = emp.State;
                                e.Status = emp.Status;
                            }
                        }
                        else
                        {
                            //Save
                            dc.Employees.Add(emp);
                        }
                        dc.SaveChanges();
                        status = true;
                    }
                }
            }
            return new JsonResult { Data = new { status = status } };
        }

        /// <summary>
        /// NOTE:  This will confirm the delete action from the database, before allow the delete action
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int id)
        {
            using (MVNDatabaseEntities dc = new MVNDatabaseEntities())
            {
                var e = dc.Employees.Where(a => a.EmployeeID == id).FirstOrDefault();
                if (e != null)
                {
                    return View(e);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteEmployee(int id)
        {
            bool status = false;
            using (MVNDatabaseEntities dc = new MVNDatabaseEntities())
            {
                var e = dc.Employees.Where(a => a.EmployeeID == id).FirstOrDefault();
                if (e != null)
                {
                    dc.Employees.Remove(e);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
}